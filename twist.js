const express = require('express');
const sqlite3 = require('sqlite3');

const srv = express();
srv.use(require('body-parser').urlencoded({ extended: false }));
srv.use(express.static('public_html'));

let db = new sqlite3.Database('x.db');

srv.listen(3030);

srv.post('/cadastrar', (req,res) => { 
    const nome = req.body.nome
    const capitulo = req.body.capitulo
    const genero = req.body.genero
    const pais = req.body.pais
    const modoleitura = req.body.modoleitura

    const sql = `insert into xmanga (nome, capitulo, genero, pais, modoleitura) values (?,?,?,?,?);`
    db.run(sql, [nome,capitulo,genero,pais,modoleitura], (erro) => {
        if(erro){
            console.log(erro.message)
        }
    })
    res.send('<h1>Cadastrado</h1>')
});
srv.post('/buscar',(req,res) => {
    const nome = req.body.nome;

    const sql = `select * from xmanga where nome = ?`;
    db.all(sql, [nome], (erro, mensagem) => {
        if(erro){
            console.log(erro.message)
        }
    res.send(`<p>${mensagem.nome}, ${mensagem.capitulo}, ${mensagem.genero}, ${mensagem.pais}, ${mensagem.modoleitura}</p>`)

    })
});